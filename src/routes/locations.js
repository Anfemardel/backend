const express = require('express');
const router = express.Router();

const mysqlConnection  = require('../database.js');

// GET all locations
router.get('/', (req, res) => {
  mysqlConnection.query('SELECT * FROM location', (err, rows, fields) => {
    if(!err) {
      res.json(rows);
    } else {
      console.log(err);
    }
  });  
});

// GET An location
router.get('/:id', (req, res) => {
  const { id } = req.params; 
  mysqlConnection.query('SELECT * FROM location WHERE id = ?', [id], (err, rows, fields) => {
    if (!err) {
      res.json(rows[0]);
    } else {
      console.log(err);
    }
  });
});

// INSERT An location
router.post('/', (req, res) => {
  const { name, area_m2} = req.body;

  mysqlConnection.query('SELECT * FROM location WHERE name = ?', [name], (err, rows, fields)=> {
    console.log(rows.length);
    debugger;
    if (rows.length < 1) {
      mysqlConnection.query('insert into location (name, area_m2) values (?,?)', [name, area_m2], (err, rows, fields) => {
        if(!err) {
          res.json({status: 'Location Saved'});
        } else {
          console.log(err);
        }
      });
    } else {
      console.log('Ya existe, no puede insertar');
    }
  });
});
module.exports = router;